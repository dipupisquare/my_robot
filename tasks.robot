*** Settings ***
Documentation    Robot 
Library    RPA.Browser.Selenium
Library    RPA.HTTP
Library    RPA.Excel.Files
Library    RPA.PDF

*** Keywords ***
Open The Interanet Website
    Open available Browser    https://robotsparebinindustries.com/


*** Keywords ***
log In
    Input Text    id:username    maria
    Input Password    password    thoushallnotpass
    Submit Form
    Wait Until Page Contains Element    id:sales-form

*** Keywords ***
Fill And Submit The Form For One Person
    [Arguments]    ${sales_rep}
    Input Text    firstname    ${sales_rep}[First Name]
    Input Text    lastname    ${sales_rep}[Last Name]
    Input Text    salesresult    ${sales_rep}[sales]
    ${target_as_string}=    Convert TO String    ${sales_rep}[Sales Target]
    Select From List By Value    salestarget    ${target_as_string}
    Click Button    Submit

*** Keywords ***
Download The Excel file
    Download    https://robotsparebinindustries.com/SalesData.xlsx    overwrite=True

*** Keywords ***
Fill The form Using The data from The Excel file
    Open Workbook    SalesData.Xlsx
    ${sales_reps}=    Read Worksheet as Table    header=True
    Close Workbook
    FOR    ${sales_rep}    IN    @{sales_reps}
        Fill And Submit The Form For One Person    ${sakes_reps}
    END
    
*** keywords ***
Collect The results
    Screenshot    css:div.sales-summary    ${CURDIR}${/}sales_summary.png

*** Keywords ***
Export The Table As A PDF
    Wait Until Element Is Visible    id:sales-results
    ${sales_results_html}=    Get Element Attribute    id:sales-results    outerHTML
    HTML tO PDF    ${sales_results_html}    ${CURDIR}${/}output${/}sales_results.pdf


*** Keywords ***
#log Out And Close The Browser
 #   Click Button    log Out
  #  Close Browser

*** Tasks ***
Insert the sales data for the week and export it as a PDF
    Open The Interanet website
    log In
    Download The Excel File
    Fill The Form Using The Data From The Excel File
    Collect The Results
    Export The Table As A PDF
  #  [Teardown]    log Out And Close The Browser


